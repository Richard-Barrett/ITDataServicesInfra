# Windows
This area is for overall adminstration and provisioning Windows Based Machines and Servers.
If your organization is using Active Directory (AD), you can view AD information on organizational users in the Terminal.

- **[Get-ADUser](http://woshub.com/get-aduser-getting-active-directory-users-data-via-powershell/)**
